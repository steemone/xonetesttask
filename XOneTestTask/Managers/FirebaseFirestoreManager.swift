import Foundation
import UIKit
import Firebase


class FirebaseFirestoreManager {
    
    public typealias GetLocationInfoCompletion = (Result<PhotoModel, Error>) -> ()
    
    
    let database = Firestore.firestore()
    static let shared = FirebaseFirestoreManager()
    
    
    func addPhotoInfo(uid: String, photoUrl: [String], locationName: String, completion: @escaping () -> ()) {
        let newDocument = database.collection(Constants.firestroneName).document(uid)
        newDocument.setData([Constants.imgUrl : photoUrl,
                             Constants.locationName : locationName]) { error in
            if let error = error {
                print(error.localizedDescription)
            } else {
                completion()
            }
        }
    }
    
    func addNewDocument(uid: String, location: String) {
        let newDocument = database.collection(Constants.firestroneName).document(uid)
        newDocument.setData([Constants.locationName : location,
                             Constants.imgUrl : [""]]) { error in
            if let error = error {
                print(error.localizedDescription)
            }
        }
    }
    
    func getLocationInfo(id: String, completion: @escaping GetLocationInfoCompletion) {
        var photo = PhotoModel()
        let document = database.collection(Constants.firestroneName).document(id)
        document.getDocument { document, error in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(error))
            }
            if let document = document, document.exists, let data = document.data() {
                photo.uid = id
                photo.locationName = data[Constants.locationName] as? String ?? ""
                photo.imgUrl = data[Constants.imgUrl] as? [String] ?? []
                photo.photo = data[Constants.photo] as? [UIImage] ?? []
                completion(.success(photo))
            }
        }
    }
}
