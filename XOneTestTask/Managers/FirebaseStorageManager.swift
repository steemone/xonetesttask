import Foundation
import FirebaseStorage

class FirebaseStorageManager {
    
    public enum StorageErrors: Error {
        case failedToUpload
        case failedToGetDownloadUrl
    }
    
    public typealias UploadPictureCompletion = (Result<String, Error>) -> ()
    public typealias DownloadUrlCompletion = (Result<UIImage, Error>) -> ()
    
    static let shared = FirebaseStorageManager()
    
    private let storage = Storage.storage().reference()
    
    public func uploadPicture(with data: Data, locationName: String, completion: @escaping UploadPictureCompletion) {
        storage.child("\(Constants.storageName)/\(locationName)").putData(data, metadata: nil) { metadata, error in
            guard error == nil else {
                print("Failed to upload picture")
                completion(.failure(StorageErrors.failedToUpload))
                return
            }
            
            self.storage.child("\(Constants.storageName)/\(locationName)").downloadURL { url, error in
                guard let url = url else {
                    print("Failed get download url")
                    completion(.failure(StorageErrors.failedToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                completion(.success(urlString))
            }
        }
    }
}
