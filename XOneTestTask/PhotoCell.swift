import UIKit

protocol PhotoCellDelegate: AnyObject {
    func returnImage(_ image: UIImage)
}

class PhotoCell: UICollectionViewCell {
    
    lazy var photoView: UIImageView = {
        let photo = UIImageView()
        photo.contentMode = .scaleAspectFill
        photo.layer.masksToBounds = true
        return photo
    }()
    
    weak var delegate: PhotoCellDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        configurePhotoConstraint()
        layer.cornerRadius = 15
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        addGestureRecognizer(tapGesture)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func imageTapped(_ sender: UITapGestureRecognizer) {
        let imageView = photoView
        guard let image = imageView.image else {return}
        self.delegate?.returnImage(image)
    }

    func configure(model: PhotoModel, index: Int){
        guard let url = URL(string: model.imgUrl[index]) else {return}
        do {
            let data = try Data(contentsOf: url)
            photoView.image = UIImage(data: data)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    private func configurePhotoConstraint() {
        contentView.addSubview(photoView)
        photoView.translatesAutoresizingMaskIntoConstraints = false
        photoView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        photoView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        photoView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        photoView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        photoView.layoutIfNeeded()
        photoView.layer.cornerRadius = 15
    }
}
