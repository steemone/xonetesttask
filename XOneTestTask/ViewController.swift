import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    struct Cells {
        static let photoCell = "PhotoCell"
    }
    
    private let uid = "12345"
    private let sectionInserts = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    private let figmaHeight: CGFloat = 1702
    private let figmaWidth: CGFloat = 750
    
    private var photoCollectionView: UICollectionView?
    private var photoModel = PhotoModel()
    
    
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = UIColor.rgbColorFor(hex: "FAFAFA")
        return scrollView
    }()
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "logoImage")
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = "ЛОКАЦИИ"
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: label.calculationHeight(50), weight: .light)
        label.textAlignment = .center
        return label
    }()
    
    lazy var shadowView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgbColorFor(hex: "FFFFFF")
        return view
    }()
    
    lazy var locationView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 23
        view.backgroundColor = UIColor.rgbColorFor(hex: "EDF3F4")
        return view
    }()
    
    lazy var locationTextView: UITextField = {
        let tv = UITextField()
        tv.text = "Название локации"
        tv.textColor = UIColor.rgbColorFor(hex: "869495")
        tv.backgroundColor = UIColor.rgbColorFor(hex: "EDF3F4")
        tv.font = UIFont.systemFont(ofSize: tv.calculationHeight(30))
        tv.textAlignment = .left
        tv.tag = 0
        return tv
    }()
    
    lazy var addButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "addButton"), for: .normal)
        button.addTarget(self, action: #selector(addPhoto(_:)), for: .touchUpInside)
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationTextView.delegate = self
        setupUI()
        downloadPhotoInfo()
    }
    
    @objc func addPhoto(_ sender: UIButton) {
        showImagePickerControllerActionSheet()
    }
    
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    private func downloadPhotoInfo() {
        FirebaseFirestoreManager.shared.getLocationInfo(id: uid) { result in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
            case .success(let model):
                self.photoModel = model
                self.locationTextView.text = model.locationName
                self.photoCollectionView?.reloadData()
            }
        }
    }
    
    private func setupUI() {
        configureScrollViewConstraint()
        configureLogoImageConstraint()
        configureTitleLabelConstraint()
        configureShadowViewConstraint()
        configureLocationViewConstraint()
        configureLocationTextViewConstraint()
        configurePhotoCollectionView()
        configurePhotoCollectionViewConstraint()
        configureAddButtonConstraint()
    }
    
    private func configureScrollViewConstraint() {
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    private func configureLogoImageConstraint() {
        scrollView.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: imageView.calculationHeight(85)).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: imageView.calculationWidth(465)).isActive = true
        imageView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 89).isActive = true
    }
    
    private func configureTitleLabelConstraint() {
        imageView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.centerXAnchor.constraint(equalTo: imageView.centerXAnchor).isActive = true
        titleLabel.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 10).isActive = true
    }
    
    private func configureShadowViewConstraint() {
        scrollView.addSubview(shadowView)
        shadowView.dropShadow(33, color: UIColor.black.cgColor , opacity: 1, offset: CGSize(width: -10, height: 16), shadowRadius: 8)
        shadowView.translatesAutoresizingMaskIntoConstraints = false
        shadowView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: shadowView.calculationHeight(71)).isActive = true
        shadowView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0).isActive = true
        shadowView.heightAnchor.constraint(equalToConstant: shadowView.calculationHeight(615)).isActive = true
        shadowView.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
    }
    
    private func configureLocationViewConstraint() {
        shadowView.addSubview(locationView)
        locationView.translatesAutoresizingMaskIntoConstraints = false
        locationView.leadingAnchor.constraint(equalTo: shadowView.leadingAnchor, constant: locationView.calculationWidth(30)).isActive = true
        locationView.topAnchor.constraint(equalTo: shadowView.topAnchor, constant: locationView.calculationHeight(32)).isActive = true
        locationView.heightAnchor.constraint(equalToConstant: locationView.calculationHeight(550)).isActive = true
        locationView.widthAnchor.constraint(equalToConstant: locationView.calculationWidth(690)).isActive = true
    }
    
    private func configureLocationTextViewConstraint() {
        locationView.addSubview(locationTextView)
        locationTextView.translatesAutoresizingMaskIntoConstraints = false
        locationTextView.topAnchor.constraint(equalTo: locationView.topAnchor, constant: locationTextView.calculationHeight(27)).isActive = true
        locationTextView.leadingAnchor.constraint(equalTo: locationView.leadingAnchor, constant: locationTextView.calculationWidth(18)).isActive = true
        locationTextView.heightAnchor.constraint(equalToConstant: locationTextView.calculationHeight(43)).isActive = true
        locationTextView.trailingAnchor.constraint(equalTo: locationView.trailingAnchor, constant: locationTextView.calculationHeight(-20)).isActive = true
    }
    
    private func configurePhotoCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        photoCollectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        guard let collect = photoCollectionView else {return}
        collect.register(PhotoCell.self, forCellWithReuseIdentifier: Cells.photoCell)
        collect.delegate = self
        collect.dataSource = self
        collect.layer.cornerRadius = 15
        collect.backgroundColor = UIColor.rgbColorFor(hex: "EDF3F4")
    }
    
    private func configurePhotoCollectionViewConstraint() {
        guard let collect = photoCollectionView else {return}
        locationView.addSubview(collect)
        collect.translatesAutoresizingMaskIntoConstraints = false
        collect.leadingAnchor.constraint(equalTo: locationView.leadingAnchor, constant: 0).isActive = true
        collect.trailingAnchor.constraint(equalTo: locationView.trailingAnchor, constant: 0).isActive = true
        collect.bottomAnchor.constraint(equalTo: locationView.bottomAnchor, constant: 0).isActive = true
        collect.topAnchor.constraint(equalTo: locationTextView.bottomAnchor, constant: collect.calculationHeight(30)).isActive = true
    }
    
    private func configureAddButtonConstraint() {
        view.addSubview(addButton)
        addButton.translatesAutoresizingMaskIntoConstraints = false
        addButton.widthAnchor.constraint(equalToConstant: addButton.calculationWidth(50)).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: addButton.calculationWidth(50)).isActive = true
        addButton.trailingAnchor.constraint(equalTo: locationView.trailingAnchor, constant: addButton.calculationWidth(-20)).isActive = true
        addButton.centerYAnchor.constraint(equalTo: locationTextView.centerYAnchor).isActive = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = locationTextView.text {
            photoModel.locationName = text
            FirebaseFirestoreManager.shared.addPhotoInfo(uid: uid, photoUrl: photoModel.imgUrl, locationName: text) {
                self.downloadPhotoInfo()
            }
        }
        textField.resignFirstResponder()
        return true
    }
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        photoModel.imgUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = photoCollectionView!.dequeueReusableCell(withReuseIdentifier: Cells.photoCell, for: indexPath) as? PhotoCell else {
            return UICollectionViewCell()
        }
        cell.configure(model: photoModel, index: indexPath.item)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemsPerRow: CGFloat = 3
        let paddingWidth = sectionInserts.left * (itemsPerRow + 1)
        let availableWidth = collectionView.frame.width - paddingWidth
        let widthPerItem = availableWidth / itemsPerRow
        let heightPerItem = widthPerItem
        return CGSize(width: widthPerItem, height: heightPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInserts.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInserts
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func showImagePickerControllerActionSheet() {
        let alert = UIAlertController(title: "Выберите вашу фотографию", message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (_) in
            self.showImagePickerController(sourceType: .camera)
        }
        alert.addAction(cameraAction)
        let galleryAction = UIAlertAction(title: "Галерея", style: .default) { (_) in
            self.showImagePickerController(sourceType: .photoLibrary)
        }
        alert.addAction(galleryAction)
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showImagePickerController(sourceType: UIImagePickerController.SourceType) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        imagePickerController.modalPresentationStyle = .currentContext
        imagePickerController.sourceType = sourceType
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            photoModel.photo.append(editImage)
            guard let imgg = editImage.pngData() else {return}
            let currentDate = Date()
            FirebaseStorageManager.shared.uploadPicture(with: imgg, locationName: "\(Constants.locationName)\(currentDate)") {  result in
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let url):
                    self.photoModel.imgUrl.append(url)
                    FirebaseFirestoreManager.shared.addPhotoInfo(uid: self.uid, photoUrl: self.photoModel.imgUrl, locationName: self.photoModel.locationName, completion: {
                        self.downloadPhotoInfo()
                    })
                }
            }
            dismiss(animated: true, completion: nil)
        }
    }
}

extension ViewController: PhotoCellDelegate {
    func returnImage(_ image: UIImage) {
        let newImageView = UIImageView(image: image)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
    }
}
