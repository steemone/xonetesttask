import UIKit
import Foundation

extension UIView {
    func dropShadow(_ radius: CGFloat = 15,color: CGColor, opacity: Float, offset: CGSize, shadowRadius: CGFloat) {
        layer.masksToBounds = false
        layer.shadowColor = color
        layer.shadowOpacity = opacity
        layer.shadowOffset = offset
        layer.shadowRadius = shadowRadius
        layer.cornerRadius = radius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
    }
}
