import UIKit

extension UIView {
    func calculationWidth(_ size: CGFloat) -> CGFloat {
        let widthScreen = UIScreen.main.bounds.width
        let figmaWidth: CGFloat = 750
        let result = widthScreen * size / figmaWidth
        return result
    }
    
    func calculationHeight(_ size: CGFloat) -> CGFloat {
        let hegihtScreen = UIScreen.main.bounds.height
        let figmaWidth: CGFloat = 1702
        let result = hegihtScreen * size / figmaWidth
        return result
    }
}
