import Foundation

public enum Constants {
    
    static let storageName = "Image_folder"
    static let firestroneName = "Database"
    static let imgUrl = "img_url"
    static let locationName = "location_name"
    static let photo = "photo"
}
