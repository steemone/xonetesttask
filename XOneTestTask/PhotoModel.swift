import UIKit

struct PhotoModel  {
    
    var locationName: String = ""
    var imgUrl: [String] = []
    var uid: String = ""
    var photo: [UIImage] = []
}
    
